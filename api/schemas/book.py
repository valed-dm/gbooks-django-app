import datetime
from dataclasses import dataclass, field
from typing import List


@dataclass
class SaleInfo:
    isbn_10: str = None
    isbn_13: str = None
    is_ebook: bool = False
    page_count: str = "no info"
    retail_price: str = "no price"
    currency: str = "no currency"
    saleability: bool = False
    sale_country: str = "no country"
    store_link: str = ""


@dataclass
class Book(SaleInfo):
    authors: List = field(default_factory=lambda: ["-no author-"])
    categories: List = field(default_factory=lambda: ["-no category-"])
    date: datetime.date = "1900-01-01"
    description: str = "no description"
    google_book_id: str = ""
    image_src: str = "/static/img/no_cover.webp"
    title: str = "no title"
    rubric: str = "read_asap"
    remark: str = ""

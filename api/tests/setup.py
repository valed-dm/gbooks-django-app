import pytest
from django.test import Client


@pytest.mark.django_db
class SetUp:
    client = Client()

    @classmethod
    def set_session(cls):
        cls.session = cls.client.session
        return cls.session

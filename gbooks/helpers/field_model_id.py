from django.db import models


def id_field():
    field = (
        "id",
        models.BigAutoField(
            auto_created=True,
            primary_key=True,
            serialize=False,
            verbose_name="ID",
        ),
    )
    return field

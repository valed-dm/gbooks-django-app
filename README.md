# gbooks-django-gitlab
### Описание проекта

Приложение для работы с Google Books API:
- простой интерфейс для подготовки запроса к API
- предварительная обработка ответа сервера с нормализацией данных
- удобная визуализация результатов поиска для работы
- просмотр подробных данных отдельных книг
- возможность сохранения отдельных книг в личную библиотеку
- переход по ссылке в интернет-магазин Google Play
- Google Play: доступны ознакомительные фрагменты, возможно приобрести легальную копи

### ToDo:
- docker hub: передать данные переменных окружения на стадии build в pipeline
- оптимизировать запросы к БД при добавлении книги в библиотеку
- использовать параметр "newest" при построении запроса к API
- продумать эффективный алгоритм нормализации данных даты выхода книги

### В рамках ДЗ №11:
- открыт аккаунт на GitLab.
- создан репозиторий для Django проекта.
- проект скопирован с GitHub.
- на локальный компьютер установлен gitlab-runner.
- на GitLab отключены shared runners, зарегистрирован assigned project runner.
- использованы пакеты pytest-django, pytest-cov.
- создан pipeline из трех стадий:
  - linting (pylint)
  - testing (pytest-django) - возвращает отчет о покрытии приложения тестами.
  - deploy (mock stage)

Pytest использован для динамического формирования названия папки для хранения artifacts.

### Сложности при выполнении ДЗ:

- работа через vpn
- неочевидный сначала и запуск, и регистрация gitlab-runner под root-ом.
- ошибка pytest-django при работе с БД (access denied) несмотря на использование 
декораторов @pytest.mark.django_db - для прохождения тестов пришлось изменить хранилище
Django session с "db" (default value) -> "cache":

```python
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
```

этот вопрос успешно решен в приложении api/tests/setup.py:
```python
@pytest.mark.django_db
class SetUp:
    client = Client()

    @classmethod
    def set_session(cls):
        cls.session = cls.client.session
        return cls.session
```
Django session storage возвращено к дефолтному значению db. Данные сессии сохраняются в sqlite.

Успешно добавлен gitlab-runner с docker executor в дополнение к shell executor.
Pipeline jobs выполняются с разными тегами.

Файл etc/gitlab-runner/config.toml:
```toml
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "dm_imac_shell"
  url = "https://gitlab.com/"
  id = 23745329
  token = "6fd_5GzoxN6mo2UYL1iz"
  token_obtained_at = 2023-05-25T14:52:17Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "shell"

[[runners]]
  name = "dm_imac_docker"
  url = "https://gitlab.com/"
  id = 23745482
  token = "ZzfK1EkyrsejQyiyPXXy"
  token_obtained_at = 2023-05-25T14:58:37Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "docker:20.10.16"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/certs/client", "/cache"]
    shm_size = 0
```
from django.shortcuts import redirect

from library.crud.save_book import save_book_to_library
from library.helpers import book_to_book_db
from library.helpers.info_add import info_add
from library.helpers.info_exists import info_exists
from library.models import Book as DB_Book


def add_book(request, book):
    user = request.user
    # checks if a book has been added to db
    book_exists = (
        DB_Book.objects.all()
        .filter(google_book_id=book["google_book_id"])
        .exists()
    )
    if book_exists:
        db_book = DB_Book.objects \
            .prefetch_related("users") \
            .get(google_book_id=book["google_book_id"])
        db_book_users = list(db_book.users.all())
        user_exists = user in db_book_users
        if user_exists:
            info_exists(request, book["title"])
            return redirect("book/")
        db_book_users.append(user)
        # m-2-m operations on book object in db are performed
        # m-2-m association tables are filled in automatically
        db_book.users.set(db_book_users)
        info_add(request, book["title"])
        return redirect("book/")

    library_book = book_to_book_db(request=request, book=book)
    save_book_to_library(library_book)
    info_add(request, book["title"])

    return book

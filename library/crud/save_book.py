from library.models import Book
from library.schemas import LibraryBook


def save_book_to_library(library_book: LibraryBook):
    book_to_library = Book(
        authors_str=library_book.authors_str,
        title=library_book.book_to_library.title,
        description=library_book.book_to_library.description,
        date=library_book.book_to_library.date,
        google_book_id=library_book.book_to_library.google_book_id,
        rubric="read_asap",
        remark="no remark added",
        image_src=library_book.image_src,
        sale_info=library_book.sale_info,
    )

    # primarily saves book_to_library data into db
    # for a reason that book_id is necessary to perform coming next m2m operations
    book_to_library.save()
    # m-2-m operations on book object in db are performed
    # m-2-m association tables are filled in automatically
    book_to_library.authors.set(library_book.authors)
    book_to_library.categories.set(library_book.categories)
    book_to_library.users.set([library_book.user])

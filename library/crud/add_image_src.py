from library.models import Image


def add_image_src(book):
    image_src = Image(image_src=book["image_src"])
    image_src.save()
    return image_src

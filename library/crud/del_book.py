from django.shortcuts import redirect

from library.helpers import info_delete
from library.models import Book as DB_Book


def delete_book(request):
    user = request.user
    delete_id = request.GET.get("del", None)
    db_book = DB_Book.objects \
        .prefetch_related("users", "image_src") \
        .get(google_book_id=delete_id)
    book_title = db_book.title
    db_book_users = list(db_book.users.all())
    if len(db_book_users) >= 2:
        db_book_users.remove(user)
        # m-2-m operations on book object in db are performed
        # m-2-m association tables are filled in automatically
        db_book.users.set(db_book_users)
        info_delete(request, book_title)
        return redirect("library/books")

    db_book.delete()
    info_delete(request, book_title)
    return redirect("library/books")

from library.models import Book


def get_books(request):
    books_data = (
        Book.objects.select_related("image_src")
        .prefetch_related("authors", "categories")
        .filter(users__username=request.user.username)
        .all()
    )

    return books_data

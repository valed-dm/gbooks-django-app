from library.models import SaleInfo


def add_sale_info(book):
    sale_info = SaleInfo(
        isbn_10=book["isbn_10"],
        isbn_13=book["isbn_13"],
        is_ebook=book["is_ebook"],
        page_count=book["page_count"],
        retail_price=book["retail_price"],
        currency=book["currency"],
        saleability=book["saleability"],
        sale_country=book["sale_country"],
        store_link=book["store_link"]
    )
    sale_info.save()
    return sale_info

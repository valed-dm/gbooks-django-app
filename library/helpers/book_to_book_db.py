from api.schemas import Book
from library.crud.add_image_src import add_image_src
from library.crud.add_sale_info import add_sale_info
from library.crud.get_or_create import get_or_create
from library.helpers.authors_str import authors_string
from library.models import Author, Category
from library.schemas import LibraryBook


def book_to_book_db(request, book) -> LibraryBook:
    book_to_library = Book()
    library_book = LibraryBook()
    user = request.user

    # truncates names to avoid doubles in db authors, categories
    book_authors = [author.strip() for author in book["authors"]]
    book_categories = [category.strip() for category in book["categories"]]

    # gets or creates authors, categories to be filled in m-2-m Book fields
    authors = get_or_create(Author, book_authors)
    categories = get_or_create(Category, book_categories)

    # still not in use at the moment but is left for the future
    authors_str = authors_string(book_authors)

    image_src = add_image_src(book)
    sale_info = add_sale_info(book)

    book_to_library.title = book["title"]
    book_to_library.description = book["description"]
    book_to_library.date = book["date"]
    book_to_library.google_book_id = book["google_book_id"]

    library_book.authors = authors
    library_book.authors_str = authors_str
    library_book.categories = categories
    library_book.image_src = image_src
    library_book.sale_info = sale_info
    library_book.user = user
    library_book.book_to_library = book_to_library

    return library_book

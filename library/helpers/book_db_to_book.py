from api.schemas import Book
from library.models import Book as DB_Book


def book_db_to_book(book: DB_Book) -> Book:
    authors = [auth.name for auth in book.authors.all()]
    categories = [cat.name for cat in book.categories.all()]

    book_data = Book(
        authors=authors,
        categories=categories,
        date=book.date,
        description=book.description,
        google_book_id=book.google_book_id,
        image_src=book.image_src.image_src,
        title=book.title,
        rubric=book.rubric,
        remark=book.remark,
        isbn_10=book.sale_info.isbn_10,
        isbn_13=book.sale_info.isbn_13,
        is_ebook=book.sale_info.is_ebook,
        page_count=book.sale_info.page_count,
        retail_price=book.sale_info.retail_price,
        currency=book.sale_info.currency,
        saleability=book.sale_info.saleability,
        sale_country=book.sale_info.sale_country,
        store_link=book.sale_info.store_link
    )

    return book_data

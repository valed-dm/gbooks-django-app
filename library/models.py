from django.contrib.auth import get_user_model
from django.db import models


# m-2-m
class Author(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return f"{self.name!r}"


# m-2-m
class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return f"{self.name!r}"


# 1-2-1
class Image(models.Model):
    image_src = models.URLField(max_length=200)

    def __str__(self):
        return f"{self.image_src!r}"


# 1-2-1
class SaleInfo(models.Model):
    isbn_10 = models.CharField(max_length=10, unique=True, null=True, default=None)
    isbn_13 = models.CharField(max_length=13, unique=True, null=True, default=None)
    is_ebook = models.BooleanField(default=False)
    page_count = models.CharField(max_length=4)
    retail_price = models.CharField(max_length=10)
    currency = models.CharField(max_length=3)
    saleability = models.BooleanField(default=False)
    sale_country = models.CharField(max_length=3)
    store_link = models.URLField(max_length=200)

    def __str__(self):
        return f"{self.store_link!r}"


# m-2-m
class Book(models.Model):
    authors_str = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True, default="")
    date = models.DateField(default="1900-01-01")
    google_book_id = models.CharField(max_length=20, unique=True)
    rubric = models.CharField(max_length=20)
    remark = models.CharField(max_length=100, blank=True, default="")
    image_src = models.OneToOneField(
        Image, null=True, related_name="images", on_delete=models.CASCADE
    )
    sale_info = models.OneToOneField(
        SaleInfo, null=True, related_name="sale_info", on_delete=models.CASCADE
    )
    authors = models.ManyToManyField(Author)
    categories = models.ManyToManyField(Category)
    users = models.ManyToManyField(get_user_model())

    def delete(self, *args, **kwargs):
        self.image_src.delete()
        self.sale_info.delete()
        return super().delete(*args, **kwargs)

    def __str__(self):
        return f"{self.title!r}"

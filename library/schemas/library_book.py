from dataclasses import dataclass, field
from typing import List

from api.schemas import Book
from library.models import Image
from library.models import SaleInfo
from userapp.models import MyUser

book = Book()
image_src = Image(image_src="/static/img/no_cover.webp")
sale_info = SaleInfo()
user = MyUser()


@dataclass
class LibraryBook:
    authors: List = field(default_factory=lambda: [])
    authors_str: str = "-no author-"
    categories: List = field(default_factory=lambda: [])
    image_src: Image = image_src
    sale_info: SaleInfo = sale_info
    user: MyUser = user
    book_to_library: Book = field(default_factory=lambda: book)

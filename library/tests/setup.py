import pytest

from library.models import Image, SaleInfo
from userapp.models import MyUser


@pytest.mark.django_db
class SetUp:
    image_src = None
    sale_info = None

    @classmethod
    def set_image(cls):
        cls.image_src = Image(image_src="/static/img/no_cover.webp")
        cls.image_src.save()
        return cls.image_src

    @classmethod
    def set_sale(cls):
        cls.sale_info = SaleInfo(
            isbn_10="1234567890",
            isbn_13="abc1234567890"
        )
        cls.sale_info.save()
        return cls.sale_info

    @classmethod
    def set_user(cls):
        cls.user = MyUser.objects.create_superuser(username='test', email='test@test.com', is_active=True)
        cls.user.set_password('Test1234')
        cls.user.save()
        return cls.user

# Generated by Django 4.2 on 2023-05-23 15:42

import django.db.models.deletion
from django.db import migrations, models

from gbooks.helpers.field_model_id import id_field


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Author",
            fields=[
                id_field(),
                ("name", models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name="Category",
            fields=[
                id_field(),
                ("name", models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name="Image",
            fields=[
                id_field(),
                ("image_src", models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name="SaleInfo",
            fields=[
                id_field(),
                (
                    "isbn_10",
                    models.CharField(
                        default=None, max_length=10, null=True, unique=True
                    ),
                ),
                (
                    "isbn_13",
                    models.CharField(
                        default=None, max_length=13, null=True, unique=True
                    ),
                ),
                ("is_ebook", models.BooleanField(default=False)),
                ("page_count", models.CharField(max_length=4)),
                ("retail_price", models.CharField(max_length=10)),
                ("currency", models.CharField(max_length=3)),
                ("saleability", models.BooleanField(default=False)),
                ("sale_country", models.CharField(max_length=3)),
                ("store_link", models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name="Book",
            fields=[
                id_field(),
                ("authors_str", models.CharField(max_length=200)),
                ("title", models.CharField(max_length=200)),
                ("description", models.TextField(blank=True, default="")),
                ("date", models.DateField(default="1900-01-01")),
                ("google_book_id", models.CharField(max_length=20, unique=True)),
                ("rubric", models.CharField(max_length=20)),
                ("remark", models.CharField(blank=True, default="", max_length=100)),
                ("authors", models.ManyToManyField(to="library.author")),
                ("categories", models.ManyToManyField(to="library.category")),
                (
                    "image_src",
                    models.OneToOneField(
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="images",
                        to="library.image",
                    ),
                ),
                (
                    "sale_info",
                    models.OneToOneField(
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="sale_info",
                        to="library.saleinfo",
                    ),
                ),
            ],
        ),
    ]
